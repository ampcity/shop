from pathlib import Path
import json

isDinnerName = True

data = {}
data['meals'] = []
ingredients = []
mealName = ""

with open('Meals.txt') as txtFile:
    for line in txtFile:
        if line.rstrip():

            line = line.rstrip()
            if isDinnerName:
                mealName = line
                isDinnerName = False
            else:
                ingredients.append(line)
        if not line.rstrip():
            isDinnerName = True
            data['meals'].append({
                'name': mealName,
                'ingredients' : ingredients
            })
            mealName = ""
            ingredients = []

with open('meal.json', 'w') as outfile:
    json.dump(data,outfile, ensure_ascii=False)


