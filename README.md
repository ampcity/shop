# Shop

Use case - My girlfriend spends about 2-3 hours a week thinking of what meals we should have before we go shopping. Which is way too long. She pretty indecisive so sometimes it can take longer. So I thought a script that could spit out random meals for the week would be helpful. She should be able to choose a certain meal she wants or get rid of random meals we are 'not feeling' that week that were selected.*Really its so I could practice scripting in `bash`🙃*

The script uses `Jq` to read `meal.json`. `meal.json` contains a list of cook-able meals with it ingredients.

Once meals are selected or randomly generated, a list of total ingredients are generated and sent to whatever emails are in `emails.txt` via `mutt`.

## Usage (Not sure why you would)

Requires

- jq `sudo apt-get install jq`
- mutt `sudo apt-get install mutt`


![](https://media.giphy.com/media/mBvqJh3CGlHsPE0Wtc/giphy.gif)
