#!/bin/bash

declare -a meals
declare -i totalmeals
declare -a allMeals

init() {
  clear
  unset meals
  unset allMeals
  if [[ -e "meal.json" ]]; then
    echo "meal.json found."
    mapfile -t allMeals <<< $(jq -cr ".meals[].name" meal.json)
    totalmeals=${#allMeals[*]}
    echo "You have ${totalmeals} meals avaliable."
  fi
}

menu() {
  echo "🥙🥗🥘🥝🥕🥖🥑🥒🥐"
  echo "        Menu       "
  echo "🥙🥗🥘🥝🥕🥖🥑🥒🥐"
  echo "1. Randomise meal plan."
  echo "2. Select meals."
  echo "3. Exit."
  declare input
 #input=2
  read -p "Enter choice [1-3] :" input
  case $input in
    1) random ;;
    2) testselect ;; #To-do Select Meals
    3) exit 0;;
    *) clear; menu ;; 
  esac

}

testselect() {
  meals+=("Spaghetti Bolognese")
  meals+=("Lasagne")
  meals+=("Vegetable lasagne")
  showMeals
}

random() { 
    read -p "How many meals are you planning to cook this week? : " numberOfMeals
 
    if [[ $numberOfMeals =~ ^-?[0-9]+$ ]] && [[ $numberOfMeals -le $totalmeals ]]; then
      printf "\n"
      local mealName
      for ((i = 0 ; i < numberOfMeals ; i++)); do
        rollMeals
      done
    else
      echo "${numberOfMeals} is not number or larger than ${totalmeals}" >&2; random
    fi
    showMeals
}

passableMeal() {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}      

rollMeals() {
  mealName=$(jq -r ".meals[$RANDOM % $totalmeals].name" meal.json)
 #if [[ ${meals[*]} =~ "${mealName}" ]] ; then
  if passableMeal "$mealName" "${meals[@]}";
  then
    rollMeals
  else
    meals+=("$mealName")
  fi

}

showMeals() {
  echo "-------------------Shopping List 🛒🚶---------------------"
  printf "\n"
  for index in ${!meals[*]}
  do
    printf "%20d: %s\n\n" $(($index + 1)) "${meals[$index]}"
  done
  submenu
}

submenu() {
  echo "Are you happy with these meals?"
  echo "-------------------------------"
  echo "1. Yes, Send me my shopping list 🛒"
  echo "2. No, I hate them all. Reroll all the meals 🔙"
  echo "3. Let me swap out some of the meals.🤔"
  echo "4. Abort! ❌"
  declare input
  read -p "Enter choice [1-4] :" input
  case $input in
    1) generatelist; sendMail;;
    2) clear; unset meals; random;;
    3) swap;; 
    4) exit 0;;
    *) clear; submenu ;; 
  esac
}

swap() {
  declare preSwapMeal
  declare postSwapMeal
  selectFromCurrentMenu "${meals[@]}"
  showMeals
}

selectFromCurrentMenu() {
  select item; do
    if [ 1 -le "$REPLY" ] && [ "$REPLY" -le $# ]; then
      preSwapMeal=$item
      echo "🔃Swap out $preSwapMeal for what meal? 🔃"
      selectFromTotalMeals "${allMeals[@]}"
      break;
    else
      echo "Select any meal from 1-$#"
    fi
  done
}

selectFromTotalMeals() {
  select item do
    if [ 1 -le "$REPLY" ] && [ "$REPLY" -le $# ]; then
      postSwapMeal=$item
      if passableMeal "$postSwapMeal" "${meals[@]}";
      then
        echo "⚠ $postSwapMeal is already on the shopping list. Choose something else."
        selectFromTotalMeals "${allMeals[@]}"
      else  
        echo "Swapping $preSwapMeal for $postSwapMeal"
        meals=("${meals[@]/$preSwapMeal/$postSwapMeal}")
        sleep 1;clear
      fi
     break;
    else
      echo "Select any meal from 1-$#"
    fi
  done
}

generatelist() {
  unset ing
  unset shoppingList
  declare -A shoppingList
  for m in "${meals[@]}"; do
    mapfile -t ing <<<$(jq -r --arg m "$m" '.meals[] | select(.name == $m ) |
      .ingredients[]' meal.json)

    for i in "${ing[@]}"; do
      if [[ ${shoppingList[$i]+_} ]];
      then
        #$((shoppingList[$i]++)) outputs stderr (n: command not found) where n is the number being increment. It works though.
        shoppingList[$i]=$(( shoppingList[$i]+1 )) # works w/o err
      else
        shoppingList[$i]=1
      fi
    done
    
  done
  echo "-------------------Ingredients List 👩---------------------" |
    tee list.txt
  printf "\n"

  for key in "${!shoppingList[@]}"; do
    printf "%40s :x%d\n" "$key" "${shoppingList[$key]}"
  done | tee -a list.txt

  printf "\n"
}

sendMail() {
  subject="Shopping list - $date"
  body="$( cat list.txt )"
  to="$( cat emails.txt )"

  echo "Sending email to: $to"
  mutt -s "$subject" $to < list.txt
}



init
menu

